package br.ucsal.bes20192.testequalidade.escola.business;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;


public class AlunoBOIntegradoTest {

//	@Before
//	public void excluirAntes() {
//		System.out.println("antes de cada teste");
//		AlunoDAO alunoDao = new AlunoDAO();	
//		alunoDao.excluirTodos();
//	}

	
	@AfterClass
	public static void excluirAlunosTeste() {
		AlunoDAO alunoDao = new AlunoDAO();	
		alunoDao.excluirTodos();
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2004 tera 16
	 * anos. Caso de teste 
	 * # | entrada 				 | saida esperada 
	 * 1 | aluno nascido em 2004 |	 * 16
	 */
	@Test
	public void testarCalculoIdadeAluno1() {
		AlunoDAO alunoDao = new AlunoDAO();
		alunoDao.excluirTodos();
		
		//AlunoBuilder builder = AlunoBuilder.alunoNascido2004();
		Aluno aluno = AlunoBuilder.alunoNascido2004().build();	
		alunoDao.salvar(aluno);
	    AlunoBO alunoBo = new AlunoBO(alunoDao, new DateHelper());
	    assertEquals(16, alunoBo.calcularIdade(aluno.getMatricula()));
	   
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		AlunoDAO alunoDao = new AlunoDAO();
		alunoDao.excluirTodos();
		
		AlunoBO alunoBo = new AlunoBO(alunoDao,new DateHelper());
		Aluno alunoEsperado = AlunoBuilder.alunoAtivo().build();
		
		alunoBo.atualizar(alunoEsperado);
		Aluno alunoAtual = alunoDao.encontrarPorMatricula(alunoEsperado.getMatricula());
		assertEquals(alunoEsperado, alunoAtual);
	}

}
