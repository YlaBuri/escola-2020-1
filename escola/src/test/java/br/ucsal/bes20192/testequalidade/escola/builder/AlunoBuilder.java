package br.ucsal.bes20192.testequalidade.escola.builder;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.domain.SituacaoAluno;

public class AlunoBuilder {
	
	
	public static final Integer MATRICUL_DEFAULT=1;
	public static final String NOME_DEFAULT="Aluno padr�o";
	public static final SituacaoAluno SITUACAO_DEFAULT=SituacaoAluno.ATIVO;
	public static final Integer ANO_NASC_DEFAULT=1999;


	private Integer matricula;
	private String nome;
	private SituacaoAluno situacao;
	private Integer anoNascimento;

	private AlunoBuilder(){}

	public static AlunoBuilder aluno() {
		return new AlunoBuilder();
	}

	public static AlunoBuilder alunoDefault() {
		return AlunoBuilder.aluno()
				.withMatricula(MATRICUL_DEFAULT)
				.withNome(NOME_DEFAULT)
				.withAnoNascimento(ANO_NASC_DEFAULT)
				.withSituacao(SITUACAO_DEFAULT);
	}
	
	public static AlunoBuilder alunoNascido2004() {
		return AlunoBuilder.alunoDefault().withAnoNascimento(2004);
	}

	public static AlunoBuilder alunoAtivo() {
		return AlunoBuilder.alunoDefault().asAtivo();
	}

//	public AlunoBuilder withMatricula(Integer matricula) {
//		this.matricula = matricula;
//		return this;
//	}

	public AlunoBuilder withNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public AlunoBuilder withMatricula(Integer matricula) {
		this.matricula = matricula;
		return this;
	}


	public AlunoBuilder withSituacao(SituacaoAluno situacao) {
		this.situacao = situacao;
		return this;
	}

	public AlunoBuilder asCancelado() {
		this.situacao = SituacaoAluno.CANCELADO;
		return this;
	}

	public AlunoBuilder asAtivo() {
		this.situacao = SituacaoAluno.ATIVO;
		return this;
	}

	public AlunoBuilder withAnoNascimento(Integer anoNascimento) {
		this.anoNascimento = anoNascimento;
		return this;
	}

	public AlunoBuilder but(){
		return AlunoBuilder.aluno()
				.withNome(this.nome)
				.withAnoNascimento(this.anoNascimento)
				.withSituacao(this.situacao);
	}

	

	public Aluno build() {
		return new Aluno(this.matricula, this.nome, this.situacao, this.anoNascimento);
	}
}
